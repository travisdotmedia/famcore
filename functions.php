<?php
/**
 * TMStarter Theme
 *
 * @package     TravisMedia\TMStarter
 * @since       1.0.2
 * @author      @travisdotmedia
 * @link        https://travis.media
 * @license     GNU General Public License 2.0+
 */
namespace TravisMedia\TMStarter;

include_once( 'lib/init.php' );

include_once( 'lib/functions/autoload.php' );
