
<?php
/**
 * TMStarter
 *
 * @package      TMStarter
 * @license      GPL-2.0+
 */
// Remove 'site-inner' from structural wrap
add_theme_support( 'genesis-structural-wraps', array( 'header', 'footer-widgets', 'footer' ) );
/**
 * Add the attributes from 'entry', since this replaces the main entry
 * 
 * @param array $attributes Existing attributes.
 * @return array Amended attributes.
 */
function tm_site_inner_attr( $attributes ) {
	
	// Add a class of 'full' for styling this .site-inner differently
	$attributes['class'] .= ' full';
	
	// Add an id of 'genesis-content' for accessible skip links
	$attributes['id'] = 'genesis-content';
	
	// Add the attributes from .entry, since this replaces the main entry
	$attributes = wp_parse_args( $attributes, genesis_attributes_entry( array() ) );
	
	return $attributes;
}
add_filter( 'genesis_attr_site-inner', 'tm_site_inner_attr' );
// Build the page
get_header();
?>

<section id="core-section" class="fc-section">
  <div class="container clearfix">
  <div class="cont s--inactive">
  <!-- cont inner start -->
  <div class="cont__inner">
    <!-- el start -->
    <div class="el">
      <div class="el__overflow">
        <div class="el__inner">
          <div class="el__bg"></div>
          <div class="el__preview-cont">
            <h2 class="el__heading">MIND</h2>
          </div>
          <div class="el__content">
            <div class="el__text">Whatever</div>
            <div class="el__close-btn"></div>
          </div>
        </div>
      </div>
      <div class="el__index">
        <div class="el__index-back"></div>
        <div class="el__index-front">
          <div class="el__index-overlay" data-index="1"></div>
        </div>
      </div>
    </div>
    <!-- el end -->
    <!-- el start -->
    <div class="el">
      <div class="el__overflow">
        <div class="el__inner">
          <div class="el__bg"></div>
          <div class="el__preview-cont">
            <h2 class="el__heading">BODY</h2>
          </div>
          <div class="el__content">
            <div class="el__text">Whatever</div>
            <div class="el__close-btn"></div>
          </div>
        </div>
      </div>
      <div class="el__index">
        <div class="el__index-back"></div>
        <div class="el__index-front">
          <div class="el__index-overlay" data-index="2"></div>
        </div>
      </div>
    </div>
    <!-- el end -->
    <!-- el start -->
    <div class="el">
      <div class="el__overflow">
        <div class="el__inner">
          <div class="el__bg"></div>
          <div class="el__preview-cont">
            <h2 class="el__heading">SPIRIT</h2>
          </div>
          <div class="el__content">
            <div class="el__text">Whatever</div>
            <div class="el__close-btn"></div>
          </div>
        </div>
      </div>
      <div class="el__index">
        <div class="el__index-back"></div>
        <div class="el__index-front">
          <div class="el__index-overlay" data-index="3"></div>
        </div>
      </div>
    </div>
    <!-- el end -->
  </div>
  <!-- cont inner end -->
</div>
  </div>
</section>

<section id="cta-section" class="fc-section">
  <div class="container aligncenter clearfix">
    <div class="first one-half cta-header">
      <h4>We're all in this together</h4>
      <p>Sign up here to get the newest material</p>
    </div>
    <div class="one-half">
      <?php echo do_shortcode('[caldera_form id="CF5b09becf0ccc7"]') ?>
    </div>
  </div>
</section>

<section id="audio-video-section" class="fc-section" style="background-color: white;">
  <div class="container clearfix">
    <div class="audio-section">
      <h3 class="audio-section-header aligncenter">Podcasts</h3>
      <div class="anchor-embeds five-sixths first">
        <div class="one-half first">
          <?php the_field('anchor_embed_1'); ?>
        </div>
        <div class="one-half">
          <?php the_field('anchor_embed_2'); ?>
        </div>
        <div class="one-half first">
          <?php the_field('anchor_embed_3'); ?>
        </div>
        <div class="one-half">
          <?php the_field('anchor_embed_4'); ?>
        </div>
      </div>
      <div class="one-sixth switch-section">
      <span class="switch">
        <span class="switch-border1">
          <span class="switch-border2">
            <input id="switch1" type="checkbox" checked />
            <label for="switch1"></label>
            <span class="switch-top"></span>
            <span class="switch-shadow"></span>
            <span class="switch-handle"></span>
            <span class="switch-handle-left"></span>
            <span class="switch-handle-right"></span>
            <span class="switch-handle-top"></span>
            <span class="switch-handle-bottom"></span>
            <span class="switch-handle-base"></span>
            <span class="switch-led switch-led-green">
              <span class="switch-led-border">
                <span class="switch-led-light">
                  <span class="switch-led-glow"></span>
                </span>
              </span>
            </span>
            <span class="switch-led switch-led-red">
              <span class="switch-led-border">
                <span class="switch-led-light">
                  <span class="switch-led-glow"></span>
                </span>
              </span>
            </span>
          </span>
        </span>
      </span>
      </div>
    </div>
  </div>
</section>

<section id="courses-section" class="fc-section">
  <div class="container clearfix">
    <div class="first one-third">
      <div class="half-circle">
        <h3>CORE GAINS</h3>
      </div>
    </div>
    <div class="two-thirds courses-testimonial-section">
      <div class="courses clearfix">
        <div class="course-1 first one-half">
          <a href="#"><img src="http://famcore.travis.media/wp-content/uploads/2018/05/course-1-image-300x200.jpeg" /></a>
          <h4>Rated G For General Audiences</h4>
        </div>
        <div class="course-2 one-half">
          <a href="#"><img src="http://famcore.travis.media/wp-content/uploads/2018/05/course-2-image-300x200.jpeg" /></a>
          <h4>Breathe Air, Swallow Food, and Think Rightly</h4>
        </div>
      </div>
      <div class="testimonial clearfix">
        <div class="testimonial-image">
          <img src="http://famcore.travis.media/wp-content/uploads/2018/05/famcore-family-testimonial-200x200.jpeg" />
        </div>
        <div class="testimonial-text">
          <div class="testimonial-header">
            <h4>"Great introduction of Apologetics for kids</h4>
          </div>
          <div class="testimonial-body">
            <p>This course was a great introduction into Apologetics for my kids. The stories intertwined into the lessons allowed my kids (ages 10 and 8) to ask questions that added more substance to these videos. My kids drew a good deal of lessons from the stories and it allowed us to continue to have great discussions inspired by these stories. I would recommend this for children 8 years and over. My 10 year old really was engaged by the lessons."</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php
get_footer();