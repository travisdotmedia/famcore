<?php
/**
 * Header HTML markup structure
 *
 * @package     TravisMedia\TMStarter
 * @since       1.0.0
 * @author      @travisdotmedia
 * @link        https://travis.media
 * @license     GNU General Public License 2.0+
 */
namespace TravisMedia\TMStarter;

/**
 * Unregister header callbacks.
 *
 * @since 1.0.0
 *
 * @return void
 */
function unregister_header_callbacks() {

}